var myApp = angular.module('grabApp', ['ngRoute', 'ui.bootstrap', 'angular-md5']);

myApp.run(function($rootScope) {
    $rootScope.apikey     = 'bccdf3c3d3b6ab54a4771c37f4764e69';
    $rootScope.privatekey = '428fc86e99673189f55e8c1439d78f39b5cb67df';
    $rootScope.url = 'http://gateway.marvel.com/v1/public/';
});


myApp.config(function($routeProvider) {
    $routeProvider
    .when('/', {
        templateUrl : 'pages/home.html',
        controller  : 'mainController'
    })
    .when('/acerca', {
        templateUrl : 'pages/about.html',
        controller  : 'aboutController'
    })
    .when('/contacto', {
        templateUrl : 'pages/contact.html',
        controller  : 'contactController'
    })
    .otherwise({
        redirectTo: '/'
    });
});

myApp.factory('hash_records', function() {
    return function() {
        return new Date().getTime();
    };
});


myApp.controller('mainController', function($scope, $rootScope, $http, md5) {
    timestamp = new Date().getTime();
    hash      = md5.createHash(timestamp+$rootScope.privatekey+$rootScope.apikey);
    $scope.url = $rootScope.url+'characters?apikey='+$rootScope.apikey+'&hash='+hash+'&ts='+timestamp+'&limit=10';

    $http({
        method : 'GET',
        url    : $scope.url,
        headers: {
            'Accept': '*/*'
        }
    }).then(function successCallback(response) {
        $scope.characters = response.data.data.results;
    });

    $scope.search_comic = function(val) {
        alert(val);
    }
});


myApp.controller('aboutController', function($scope) {
    $scope.message = 'Esta es la página "Acerca de"';
});

myApp.controller('contactController', function($scope) {
    $scope.message = 'Esta es la página de "Contacto", aquí podemos poner un formulario';
});
